import json

from .settings import *

SECRET_KEY = '${cfg:secretkey}'
DEBUG = json.loads('${cfg:debug}')
TEMPLATE_DEBUG = DEBUG
ALLOWED_HOSTS = ['${cfg:domain}']

DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.${cfg:dbengine}',
        'NAME':     '${cfg:dbname}',
        'USER':     '${cfg:dbuser}',
        'PASSWORD': '${cfg:dbpass}',
        'HOST':     '${cfg:dbhost}',
        'PORT':     '${cfg:dbport}',
    }
}
