from setuptools import find_packages
from setuptools import setup


setup(
    name='kelnero',
    version='0.1',
    license='AGPL',
    packages=find_packages('src'),
    include_package_data=True,
    zip_safe=False,
    package_dir={'': 'src'},
    install_requires=[
        'django',
        'south',
        'django-mptt',
        'django-crispy-forms',
        'psycopg2',
        'pathlib',
    ],
)
