from django.contrib import admin

from mptt.admin import MPTTModelAdmin

from .models import Item
from .models import Table
from .models import Waiter
from .models import Client
from .models import Order
from .models import OrderStatus
from .models import Bill



class ItemAdmin(MPTTModelAdmin):
    list_display = ('name', 'treename', 'price')

admin.site.register(Item, ItemAdmin)


class TableAdmin(admin.ModelAdmin):
    pass
admin.site.register(Table, TableAdmin)


class WaiterAdmin(admin.ModelAdmin):
    pass
admin.site.register(Waiter, WaiterAdmin)


class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'table', 'friend')

    def table(self, obj):
        return obj.bill.code

admin.site.register(Client, ClientAdmin)


class OrderAdmin(admin.ModelAdmin):
    pass
admin.site.register(Order, OrderAdmin)


class OrderStatusAdmin(admin.ModelAdmin):
    pass
admin.site.register(OrderStatus, OrderStatusAdmin)


class BillAdmin(admin.ModelAdmin):
    list_display = ('table_name', 'code')

    def table_name(self, obj):
        return obj.table.name

admin.site.register(Bill, BillAdmin)
