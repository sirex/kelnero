# coding: utf-8

from django.utils.translation import ugettext_lazy as _

from .models import Client
from .models import Order
from .models import OrderStatus


def create_client(bill, client_name):
    return Client.objects.create(bill=bill, name=client_name.lower())


def add_friend(client, friend_name):
    friend = create_client(client.bill, friend_name)
    friend.friend = client
    friend.save()
    return friend


def create_order(client, item, status=Order.CREATED):
    order = Order.objects.create(client=client, item=item, status=status)
    OrderStatus.objects.create(order=order, status=status)
    return order


def update_order(order, status=None, note=None):
    updated = False

    if status is not None and order.status != status:
        OrderStatus.objects.create(order=order, status=status)
        order.status = status
        updated = True

    if note is not None:
        order.note = note
        updated = True

    if updated:
        order.save()


def delete_order(order):
    assert order.status == Order.CREATED
    order.delete()


def cancel_order(order, reason=''):
    order.cancel_reason = reason
    order.status = Order.CANCELED
    order.save()
    OrderStatus.objects.create(order=order, status=Order.CANCELED,
                               cancel_reason=reason)


def set_items_ended(parent_item, cancel_orders=False):
    for item in parent_item.get_descendants(include_self=True):
        item.ended = True
        item.save()

        if cancel_orders:
            for order in Order.objects.filter(item=item):
                cancel_order(
                    order, reason=_(u'Atsiprašome, šio gaminio nebeturime.')
                )



def confirm_orders(client):
    for order in Order.objects.filter(client=client, status=Order.CREATED):
        update_order(order, status=Order.CONFIRMED)


def open_bill(bill):
    bill.active = True
    bill.save()


def close_bill(bill):
    bill.active = False
    bill.save()


def bill_payment(bill):
    orders = (
        Order.objects.
        filter(client__bill=bill, status=Order.DELIVERED)
    )
    for order in orders:
        update_order(order, Order.PAID)
    bill.active = False
    bill.save()


def client_payment(client):
    orders = (
        Order.objects.
        filter(client=client, status=Order.DELIVERED)
    )
    for order in orders:
        update_order(order, Order.PAID)
