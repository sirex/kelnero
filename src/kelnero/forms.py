# coding: utf-8

from django import forms
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext
from django.contrib.auth.forms import AuthenticationForm
from django.core.urlresolvers import reverse

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from crispy_forms.layout import Hidden

from .models import Client
from .models import Bill


class EnterBillForm(forms.Form):
    code = forms.CharField(
        label=_(u'Įveskite staliuko numerį'),
        widget=forms.TextInput(attrs={'type': 'number'}),
    )

    def __init__(self, *args, **kwargs):
        super(EnterBillForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Hidden('action', 'new_client'))
        self.helper.add_input(Submit('submit', _('Pasirinkti')))

    def clean_code(self):
        code = self.cleaned_data.get('code')
        if code and not Bill.objects.filter(code=code, active=True).exists():
            raise forms.ValidationError(ugettext(
                u'Staliuko, su tokiu numeriu, nėra.'
            ))
        return code


class ExistingBillForm(forms.Form):
    bill_code = forms.CharField(
        label=_(u'Įveskite staliuko numerį'),
        widget=forms.TextInput(attrs={'type': 'number'}),
    )
    client_code = forms.CharField(
        label=_(u'Jūsų sąskaitos kodas'),
        widget=forms.TextInput(attrs={'type': 'number'}),
    )

    def __init__(self, *args, **kwargs):
        super(ExistingBillForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Hidden('action', 'old_client'))
        self.helper.add_input(Submit('submit', _('Prisijungti')))

    def clean_bill_code(self):
        code = self.cleaned_data.get('bill_code')
        if code and not Bill.objects.filter(code=code, active=True).exists():
            raise forms.ValidationError(ugettext(
                u'Staliuko, su tokiu numeriu, nėra.'
            ))
        return code

    def clean_client_code(self):
        bill_code = self.cleaned_data.get('bill_code')
        client_code = self.cleaned_data.get('client_code')
        client_qs = (
            Client.objects.filter(bill__code=bill_code, code=client_code)
        )
        if bill_code and client_code and not client_qs.exists():
            raise forms.ValidationError(ugettext(
                u'Sąskaitos su tokiu kodu, nėra.'
            ))
        return client_code


class NewClientForm(forms.Form):
    name = forms.CharField(
        label=_(u'Nurodykite savo vardą arba slapyvardį'),
        help_text=_(u'Šiuo vardu bus nešami užsakymai.'),
    )

    def __init__(self, bill, *args, **kwargs):
        super(NewClientForm, self).__init__(*args, **kwargs)
        self.bill = bill
        self.helper = FormHelper()
        self.helper.add_input(self.get_submit_button())

    def get_submit_button(self):
        return Submit('submit', _(u'Saugoti'))

    def clean_name(self):
        name = self.cleaned_data.get('name').lower()
        is_client_exitsts = (
            Client.objects.filter(bill=self.bill, name=name).exists()
        )
        if name and is_client_exitsts:
            raise forms.ValidationError(ugettext(
                u'Žmogus tokiu pačiu vardu jau yra užsiregistravęs '
                u'prie šio staliuko, nurodykite kitokį vardą.'
            ))
        return name


class NewFriendForm(NewClientForm):
    name = forms.CharField(
        label=_(u'Draugo vardas arba slapyvardis'),
        help_text=_(u'Šiuo vardu bus nešami užsakymai.'),
    )

    def get_submit_button(self):
        return Submit('submit', _(u'Pridėti'))


class LoginForm(AuthenticationForm):
    username = forms.CharField(label=_(u'Naudotojo vardas'), max_length=254)
    password = forms.CharField(label=_(u'Slaptažodis'), widget=forms.PasswordInput)
    next = forms.CharField(widget=forms.HiddenInput())

    error_messages = {
        'invalid_login': _(
            u'Neteisingas naudotojo vardas arba slaptažodis. Abiejuose '
            u'laukuose didžiosios ir mažosios raidės skiriasi.'
        ),
        'inactive': _('Ši naudotojo paskyra yra neaktyvi.'),
    }

    def __init__(self, request, *args, **kwargs):
        next_url = request.GET.get('next', reverse('waiter'))
        kwargs.setdefault('initial', {})['next'] = next_url
        super(LoginForm, self).__init__(request, *args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', _(u'Prisijungti')))
