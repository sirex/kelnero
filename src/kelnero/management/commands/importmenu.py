import json

from django.core.management.base import BaseCommand

from kelnero.models import Item


class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, datafile, *args, **options):
        with open(datafile) as f:
            data = json.load(f)

        Item.objects.all().delete()

        objmap = dict()
        for item in data:
            id = item.pop('id')
            parent = item.pop('parent')
            if parent > 0:
                item['parent_id'] = objmap[parent]
            obj = Item.objects.create(**item)
            objmap[id] = obj.pk
