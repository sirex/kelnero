# coding: utf-8
from __future__ import unicode_literals

from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse

from kelnero.utils.navigation import item
from kelnero.utils.navigation import select_active


def codes(active=None):
    return select_active(active, [
        item('summary', _('Suvestinė'), reverse('manager_codes_summary')),
        item('codes', _('Kodai'), reverse('manager_codes_code_list')),
    ])
