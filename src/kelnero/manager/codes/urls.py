from kelnero.utils.urls import patterns_


urlpatterns = patterns_(
    'manager.codes',
    ('code_list', ''),
    ('summary', 'summary/'),
)
