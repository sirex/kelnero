from django.shortcuts import render

from kelnero import navigation


def code_list(request):
    return render(request, 'kelnero/manager/codes/code_list.html', dict(
        navbar=navigation.menu('codes'),
    ))


def summary(request):
    return render(request, 'kelnero/manager/codes/summary.html', dict(
        navbar=navigation.menu('codes'),
    ))
