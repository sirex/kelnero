# coding: utf-8
from __future__ import unicode_literals

from django import forms

from kelnero.models import Item


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ('name', 'treename')


class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ('name', 'treename', 'price', 'ended')

    def __init__(self, *args, **kwargs):
        super(ItemForm, self).__init__(*args, **kwargs)
        self.fields['price'].required = True
