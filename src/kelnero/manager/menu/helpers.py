from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse

from kelnero.models import Item


def get_parent(parent_id):
    return get_object_or_404(Item, pk=parent_id) if parent_id else None


def category_return_url(request, form):
    ref = request.GET.get('ref')
    if ref:
        return ref
    elif form.instance and form.instance.pk:
        return reverse('manager_menu_items', kwargs=dict(
            parent_id=form.instance.pk
        ))
    else:
        return reverse('manager_menu_items')


def item_return_url(request, form):
    ref = request.GET.get('ref')
    if ref:
        return ref
    elif form.instance and form.instance.pk:
        return reverse('manager_menu_items', kwargs=dict(
            parent_id=form.instance.parent.pk
        ))
    else:
        return reverse('manager_menu_items')
