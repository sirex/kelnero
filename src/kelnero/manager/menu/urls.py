from kelnero.utils.urls import patterns_


urlpatterns = patterns_(
    'manager.menu',
    ('items', ''),
    ('items', '(?P<parent_id>\d+)/'),
    ('category_create', 'categories/create/'),
    ('category_create', 'categories/create/(?P<parent_id>\d+)/'),
    ('category_update', 'categories/(?P<item_id>\d+)/update/'),
    ('item_details', 'items/(?P<item_id>\d+)/'),
    ('item_create', 'items/create/'),
    ('item_create', 'items/create/(?P<parent_id>\d+)/'),
    ('item_update', 'items/(?P<item_id>\d+)/update/'),
)
