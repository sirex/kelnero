# coding: utf-8
from __future__ import unicode_literals

from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext
from django.core.urlresolvers import reverse

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout
from crispy_forms.layout import HTML
from crispy_forms.bootstrap import FormActions
from crispy_forms.bootstrap import AppendedText

from kelnero import navigation
from kelnero.models import Item
from kelnero.utils.crispyforms import Cancel
from kelnero.utils.crispyforms import Submit
from kelnero.utils.referer import referer

from kelnero.manager.menu.helpers import get_parent
from kelnero.manager.menu.helpers import category_return_url
from kelnero.manager.menu.helpers import item_return_url
from kelnero.manager.menu.forms import CategoryForm
from kelnero.manager.menu.forms import ItemForm


def items(request, parent_id=None):
    parent = get_parent(parent_id)
    return render(request, 'kelnero/manager/menu/items.html', dict(
        navbar=navigation.menu('menu'),
        parent=parent,
        items=Item.objects.filter(parent=parent).order_by('name'),
    ))


def item_details(request, item_id=None):
    item = get_object_or_404(Item, pk=item_id)
    return render(request, 'kelnero/manager/menu/item_details.html', dict(
        navbar=navigation.menu('menu'),
        item=item,
    ))


def category_create(request, parent_id=None):
    parent = get_parent(parent_id)

    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.parent = parent
            item.save()
            if request.POST.get('action') == 'add-more':
                return redirect('%s?%s' % (
                    reverse('manager_menu_category_create', args=(parent.pk,)),
                    referer(request),
                ))
            else:
                return redirect(category_return_url(request, form))
    else:
        form = CategoryForm()

    form.helper = FormHelper()
    form.helper.layout = Layout(
        Layout(*CategoryForm.Meta.fields),
        HTML('<hr>'),
        FormActions(
            Submit(
                ugettext('Išsaugoti ir pridėti naują'),
                name='action', value='add-more',
            ),
        ),
        FormActions(
            Submit(ugettext('Išsaugoti')),
            Cancel(category_return_url(request, form)),
        ),
    )

    return render(request, 'kelnero/manager/menu/category_form.html', dict(
        navbar=navigation.menu('menu'),
        form=form,
    ))


def category_update(request, item_id):
    item = get_object_or_404(Item, pk=item_id)

    if request.method == 'POST':
        form = CategoryForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect(category_return_url(request, form))
    else:
        form = CategoryForm(instance=item)

    form.helper = FormHelper()
    form.helper.layout = Layout(
        Layout(*CategoryForm.Meta.fields),
        HTML('<hr>'),
        FormActions(
            Submit(ugettext('Išsaugoti')),
            Cancel(category_return_url(request, form)),
        ),
    )

    return render(request, 'kelnero/manager/menu/category_form.html', dict(
        navbar=navigation.menu('menu'),
        form=form,
    ))


def item_create(request, parent_id=None):
    parent = get_parent(parent_id)

    if request.method == 'POST':
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.parent = parent
            item.save()
            if request.POST.get('action') == 'add-more':
                return redirect('%s?%s' % (
                    reverse('manager_menu_item_create', args=(parent.pk,)),
                    referer(request),
                ))
            else:
                return redirect(item_return_url(request, form))
    else:
        form = ItemForm()

    form.helper = FormHelper()
    form.helper.layout = Layout(
        Layout(*ItemForm.Meta.fields),
        HTML('<hr>'),
        FormActions(
            Submit(
                ugettext('Išsaugoti ir pridėti naują'),
                name='action', value='add-more',
            ),
        ),
        FormActions(
            Submit(ugettext('Išsaugoti')),
            Cancel(item_return_url(request, form)),
        ),
    )
    form.helper['price'].wrap(AppendedText, 'Lt')

    return render(request, 'kelnero/manager/menu/item_form.html', dict(
        navbar=navigation.menu('menu'),
        form=form,
    ))


def item_update(request, item_id=None):
    item = get_object_or_404(Item, pk=item_id)

    if request.method == 'POST':
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect(item_return_url(request, form))
    else:
        form = ItemForm(instance=item)

    form.helper = FormHelper()
    form.helper.layout = Layout(
        Layout(*ItemForm.Meta.fields),
        HTML('<hr>'),
        FormActions(
            Submit(ugettext('Išsaugoti')),
            Cancel(item_return_url(request, form)),
        ),
    )
    form.helper['price'].wrap(AppendedText, 'Lt')

    return render(request, 'kelnero/manager/menu/item_form.html', dict(
        navbar=navigation.menu('menu'),
        form=form,
    ))
