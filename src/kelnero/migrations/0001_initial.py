# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Item'
        db.create_table(u'kelnero_item', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='children', null=True, to=orm['kelnero.Item'])),
            ('price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=5, decimal_places=2, blank=True)),
            ('lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
        ))
        db.send_create_signal(u'kelnero', ['Item'])

        # Adding model 'Table'
        db.create_table(u'kelnero_table', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128)),
        ))
        db.send_create_signal(u'kelnero', ['Table'])

        # Adding model 'Bill'
        db.create_table(u'kelnero_bill', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('table', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kelnero.Table'])),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=16, blank=True)),
        ))
        db.send_create_signal(u'kelnero', ['Bill'])

        # Adding model 'Waiter'
        db.create_table(u'kelnero_waiter', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'kelnero', ['Waiter'])

        # Adding model 'Client'
        db.create_table(u'kelnero_client', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('bill', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kelnero.Bill'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=16, blank=True)),
            ('key', self.gf('django.db.models.fields.CharField')(unique=True, max_length=12, blank=True)),
            ('friend', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kelnero.Client'], null=True, blank=True)),
        ))
        db.send_create_signal(u'kelnero', ['Client'])

        # Adding unique constraint on 'Client', fields ['bill', 'name']
        db.create_unique(u'kelnero_client', ['bill_id', 'name'])

        # Adding model 'Order'
        db.create_table(u'kelnero_order', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kelnero.Client'])),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kelnero.Item'])),
            ('note', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'kelnero', ['Order'])

        # Adding model 'OrderStatus'
        db.create_table(u'kelnero_orderstatus', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('waiter', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kelnero.Waiter'], null=True)),
            ('order', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['kelnero.Order'])),
            ('status', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'kelnero', ['OrderStatus'])


    def backwards(self, orm):
        # Removing unique constraint on 'Client', fields ['bill', 'name']
        db.delete_unique(u'kelnero_client', ['bill_id', 'name'])

        # Deleting model 'Item'
        db.delete_table(u'kelnero_item')

        # Deleting model 'Table'
        db.delete_table(u'kelnero_table')

        # Deleting model 'Bill'
        db.delete_table(u'kelnero_bill')

        # Deleting model 'Waiter'
        db.delete_table(u'kelnero_waiter')

        # Deleting model 'Client'
        db.delete_table(u'kelnero_client')

        # Deleting model 'Order'
        db.delete_table(u'kelnero_order')

        # Deleting model 'OrderStatus'
        db.delete_table(u'kelnero_orderstatus')


    models = {
        u'kelnero.bill': {
            'Meta': {'object_name': 'Bill'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'table': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['kelnero.Table']"})
        },
        u'kelnero.client': {
            'Meta': {'unique_together': "(('bill', 'name'),)", 'object_name': 'Client'},
            'bill': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['kelnero.Bill']"}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'friend': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['kelnero.Client']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '12', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'kelnero.item': {
            'Meta': {'object_name': 'Item'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['kelnero.Item']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '5', 'decimal_places': '2', 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'kelnero.order': {
            'Meta': {'object_name': 'Order'},
            'client': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['kelnero.Client']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['kelnero.Item']"}),
            'note': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'kelnero.orderstatus': {
            'Meta': {'object_name': 'OrderStatus'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['kelnero.Order']"}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'waiter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['kelnero.Waiter']", 'null': 'True'})
        },
        u'kelnero.table': {
            'Meta': {'object_name': 'Table'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        },
        u'kelnero.waiter': {
            'Meta': {'object_name': 'Waiter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['kelnero']