# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Item.treename'
        db.add_column(u'kelnero_item', 'treename',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)


        # # Changing field 'OrderStatus.created'
        # db.alter_column(u'kelnero_orderstatus', 'created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

        # # Changing field 'Client.created'
        # db.alter_column(u'kelnero_client', 'created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

        # # Changing field 'Bill.created'
        # db.alter_column(u'kelnero_bill', 'created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

        # # Changing field 'Order.created'
        # db.alter_column(u'kelnero_order', 'created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default='2010-01-01 01:01:01'))

    def backwards(self, orm):
        # Deleting field 'Item.treename'
        db.delete_column(u'kelnero_item', 'treename')


        # Changing field 'OrderStatus.created'
        db.alter_column(u'kelnero_orderstatus', 'created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True))

        # Changing field 'Client.created'
        db.alter_column(u'kelnero_client', 'created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True))

        # Changing field 'Bill.created'
        db.alter_column(u'kelnero_bill', 'created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True))

        # Changing field 'Order.created'
        db.alter_column(u'kelnero_order', 'created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, null=True))

    models = {
        u'kelnero.bill': {
            'Meta': {'object_name': 'Bill'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'table': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['kelnero.Table']"})
        },
        u'kelnero.client': {
            'Meta': {'unique_together': "(('bill', 'name'),)", 'object_name': 'Client'},
            'bill': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['kelnero.Bill']"}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'friend': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['kelnero.Client']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '12', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'kelnero.item': {
            'Meta': {'object_name': 'Item'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['kelnero.Item']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '5', 'decimal_places': '2', 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'treename': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128', 'blank': 'True'})
        },
        u'kelnero.order': {
            'Meta': {'object_name': 'Order'},
            'client': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['kelnero.Client']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['kelnero.Item']"}),
            'note': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'kelnero.orderstatus': {
            'Meta': {'object_name': 'OrderStatus'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['kelnero.Order']"}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'waiter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['kelnero.Waiter']", 'null': 'True'})
        },
        u'kelnero.table': {
            'Meta': {'object_name': 'Table'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        },
        u'kelnero.waiter': {
            'Meta': {'object_name': 'Waiter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['kelnero']
