# coding: utf-8
from __future__ import unicode_literals

import string

from django.db import models
from django.utils.translation import ugettext_lazy as _

from mptt.models import MPTTModel
from mptt.models import TreeForeignKey

from kelnero.utils.gencode import gen_code


class Item(MPTTModel):
    name = models.CharField(_('Pavadinimas'), max_length=128)
    treename = models.CharField(
        _('Pavadinimas sąraše'), max_length=128, default='', blank=True,
        help_text=_(
            'Pavadinimas sąraše turi būti sutrumpintas pavadinimo variantas. '
            'Jei nenurodyta, bus naudojamas paprastas pavadinimas.'
        ),
    )
    parent = TreeForeignKey(
        'self', null=True, blank=True, related_name='children'
    )
    price = models.DecimalField(
        _('Kaina'),
        max_digits=5, decimal_places=2, null=True, blank=True
    )
    ended = models.BooleanField(_('Baigėsi'), default=False, help_text=_(
        'Jei pažymėta, reiškia, kad produktas baigėsi. Pasibaigę produktai '
        'nerodomi meniu. Visiems užsisakiusiems pranešama, kad šis '
        'produktas baigėsi ir nebus pateiktas.'
    ))

    class MPTTMeta:
        order_insertion_by = ['name']

    def __unicode__(self):
        return self.name

    def get_indent(self):
        return '%dpx' % (((self.level + 1) * 10) + 10)

    def get_tree_name(self):
        return self.treename or self.name

    def is_category(self):
        return self.price is None


class Table(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __unicode__(self):
        return self.name


class Bill(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=False)
    table = models.ForeignKey(Table)
    code = models.CharField(max_length=16, blank=True, unique=True)

    def set_random_code(self):
        code = gen_code()
        while Bill.objects.filter(code=code).exists():
            code = gen_code()
        self.code = code

    def save(self, *args, **kwargs):
        if self.code == '':
            self.set_random_code()
        super(Bill, self).save(*args, **kwargs)


class Waiter(models.Model):
    name = models.CharField(max_length=128)

    def __unicode__(self):
        return self.name


class Client(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    bill = models.ForeignKey(Bill)
    name = models.CharField(max_length=128)
    code = models.CharField(max_length=16, blank=True)
    key = models.CharField(max_length=12, blank=True, unique=True, editable=False)
    friend = models.ForeignKey('self', null=True, blank=True)

    class Meta:
        unique_together = (
            ('bill', 'name'),
            ('bill', 'code'),
        )

    def __unicode__(self):
        return self.name

    def set_random_code(self):
        self.code = gen_code()

    def set_random_key(self):
        size = 12
        alphabet = string.digits + string.ascii_lowercase
        key = gen_code(size, alphabet)
        while Client.objects.filter(key=key).exists():
            key = gen_code(size, alphabet)
        self.key = key

    def save(self, *args, **kwargs):
        if self.code == '':
            self.set_random_code()
        if self.key == '':
            self.set_random_key()
        super(Client, self).save(*args, **kwargs)


class Order(models.Model):
    CREATED   = 0
    CONFIRMED = 1
    ACCEPTED  = 2
    DELIVERED = 3
    PAID      = 4
    CANCELED  = 5
    STATUS_CHOICES = (
        (CREATED,   _('Nepatvirtintas')),
        (CONFIRMED, _('Užsakyta')),
        (ACCEPTED,  _('Priimta')),
        (DELIVERED, _('Atnešta')),
        (PAID,      _('Sumokėta')),
        (CANCELED,  _('Atšauktas')),
    )

    created = models.DateTimeField(auto_now_add=True)
    client = models.ForeignKey(Client)
    item = models.ForeignKey(Item)
    note = models.TextField(default='', blank=True)
    status = models.PositiveSmallIntegerField(
        choices=STATUS_CHOICES, default=CREATED
    )
    cancel_reason = models.TextField(blank=True)


class OrderStatus(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    waiter = models.ForeignKey(Waiter, null=True)
    order = models.ForeignKey(Order)
    status = models.PositiveSmallIntegerField(choices=Order.STATUS_CHOICES)
    cancel_reason = models.TextField(blank=True)
