# coding: utf-8
from __future__ import unicode_literals

from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse

from kelnero.utils.navigation import item
from kelnero.utils.navigation import select_active


def client(client, active=None):
    link = lambda name, *args: reverse(name, args=([client.key] + list(args)))
    return select_active(active, [
        item('orders', _('Užsakymai'), link('client_orders')),
        item('friends', _('Draugai'), link('friends')),
        item('client', client.name.title(), link('client_details')),
    ])


def waiter_main_menu(active=None):
    return item(
        '',
        '<span class="glyphicon glyphicon-chevron-down"></span>',
        right=True,
        children=select_active(active, [
            item('orders', _('Užsakymai'), reverse('waiter')),
            item('bills', _('Sąskaitos'), reverse('bills')),
            item('menu', _('Meniu'), reverse('manager_menu_items')),
            item('codes', _('Kodai'), reverse('manager_codes_code_list')),
            item('logout', _('Atsijungti'), reverse('logout')),
        ]),
    )


def waiter(active=None):
    return select_active(active, [
        item('orders', _('Užsakymai'), reverse('waiter')),
        item('bills', _('Sąskaitos'), reverse('bills')),
        waiter_main_menu(active)
    ])


def menu(active=None):
    return select_active(active, [
        item('menu', _('Meniu'), reverse('manager_menu_items')),
        item('codes', _('Kodai'), reverse('manager_codes_code_list')),
        waiter_main_menu(active)
    ])
