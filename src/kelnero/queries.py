from django.db.models import Q

from .models import Client


def get_active_client(client_id):
    if client_id is None:
        return None

    try:
        return Client.objects.get(pk=client_id)
    except Client.DoesNotExist:
        return None


def query_all_friends(client):
    if client.friend:
        host = client.friend
    else:
        host = client
    return Client.objects.filter(Q(pk=host.pk) | Q(friend=host))
