from kelnero.settings.base import *  # noqa

SECRET_KEY = '3qzu!xlx95gj+42jken3d&1)dxisbtyj4kp5wkq_a-yg6iweja'
DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.sqlite3',
        'NAME':     'db.sqlite3',
        'USER':     '',
        'PASSWORD': '',
        'HOST':     '',
        'PORT':     '',
    }
}
