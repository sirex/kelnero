from django import template
from django.utils.module_loading import import_by_path

from kelnero.utils.referer import referer

register = template.Library()


@register.simple_tag(takes_context=True)
def ref(context):
    return referer(context['request'])


@register.inclusion_tag('inclusiontags/pills.html')
def pills(menu_path, active=None):
    menu = import_by_path(menu_path)
    return dict(
        items=menu(active)
    )
