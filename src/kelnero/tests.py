from django.test import TestCase
from django.contrib.auth.models import User

from .models import Client
from .models import Table
from .models import Bill
from .models import Item
from .models import Order
from . import commands


class FullWorkFlowTest(TestCase):
    def test_full_workflow(self):
        table = Table.objects.create(name='1')
        bill = Bill.objects.create(table=table, active=True)
        beer = Item.objects.create(name='Beer')
        busi3 = Item.objects.create(name='Busi trecias', parent=beer)
        halfl = Item.objects.create(name='0.5 l', parent=busi3, price='7')


        # 1. Visit home page
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)


        # 2. Select table bill
        response = self.client.post('/', dict(action='new_client', code=bill.code))
        self.assertRedirects(response, '/bill/%s/new-client/' % bill.code)


        # 3. Enter name
        response = self.client.post(
            '/bill/%s/new-client/' % bill.code, dict(name='Vardenis')
        )
        client = Client.objects.get(bill=bill, name='vardenis')
        self.assertRedirects(response, '/client/%s/orders/' % client.key)


        # 4. Browse to an item
        response = self.client.get(
            '/client/%s/items/%s/' % (client.key, busi3.pk),
        )
        self.assertEqual(response.status_code, 200)


        # 5. Add item to your orders
        response = self.client.post(
            '/client/%s/items/%s/' % (client.key, busi3.pk),
            dict(item=halfl.pk),
        )
        self.assertRedirects(response, '/client/%s/orders/' % client.key)

        orders = list(Item.objects.filter(order__client=client))
        self.assertEqual(orders, [halfl])

        statuses = list(
            Order.objects.filter(client=client).
            values_list('status', flat=True)
        )
        self.assertEqual(statuses, [Order.CREATED])


        # 6. Confirm your orders
        response = self.client.post(
            '/client/%s/orders/' % client.key, dict(action='confirm'),
        )
        self.assertRedirects(response, '/client/%s/orders/' % client.key)

        statuses = list(
            Order.objects.filter(client=client).
            values_list('status', flat=True)
        )
        self.assertEqual(statuses, [Order.CONFIRMED])


        # 7. Visit friends page
        response = self.client.post('/client/%s/friends/' % client.key)
        self.assertEqual(response.status_code, 200)


        # 8. Visit add friend form
        response = self.client.post('/client/%s/friends/add/' % client.key)
        self.assertEqual(response.status_code, 200)


        # 9. Add friend
        response = self.client.post(
            '/client/%s/friends/add/' % client.key, dict(name='Draugelis'),
        )
        self.assertRedirects(response, '/client/%s/friends/' % client.key)

        friend = Client.objects.get(friend=client)
        self.assertEqual(friend.name, 'draugelis')


        # Create waiter account
        User.objects.create_user(username='waiter', password='secret')
        self.client.login(username='waiter', password='secret')


        # 10. Visit list of tables
        response = self.client.get('/bills/')
        self.assertEqual(response.status_code, 200)


        # 11. Visit bill orders page
        response = self.client.get('/bills/%s/orders/' % bill.pk)
        self.assertEqual(response.status_code, 200)


        # 12. Accept order
        order = client.order_set.get()
        response = self.client.post(
            '/bills/%s/orders/' % bill.pk, dict(order=order.pk)
        )
        self.assertRedirects(response, '/bills/%s/orders/' % bill.pk)
        self.assertEqual(Order.objects.get(pk=order.pk).status, Order.ACCEPTED)


        # 12. Deliver order
        response = self.client.post(
            '/bills/%s/orders/' % bill.pk, dict(order=order.pk)
        )
        self.assertRedirects(response, '/bills/%s/orders/' % bill.pk)
        self.assertEqual(Order.objects.get(pk=order.pk).status, Order.DELIVERED)


        # 13. Show bill
        response = self.client.get('/bills/%s/' % bill.pk)
        self.assertEqual(response.status_code, 200)


        # 14. Accept bill
        response = self.client.post(
            '/bills/%s/' % bill.pk, dict(action='paid')
        )
        self.assertRedirects(response, '/bills/%s/' % bill.pk)

    def test_cancel_order(self):
        table = Table.objects.create(name='2')
        bill = Bill.objects.create(table=table, active=True)
        item = Item.objects.create(name='Beer', price=7)
        client = commands.create_client(bill, 'client1')
        order = commands.create_order(client, item)

        User.objects.create_user(username='waiter', password='secret')
        self.client.login(username='waiter', password='secret')


        # 1. Vsit order page
        response = self.client.get(
            '/bills/%s/orders/%s/' % (bill.pk, order.pk)
        )
        self.assertEqual(response.status_code, 200)


        # 2. Cancel order
        response = self.client.post(
            '/bills/%s/orders/%s/' % (bill.pk, order.pk), dict(action='cancel')
        )
        self.assertRedirects(
            response, '/bills/%s/orders/%s/' % (bill.pk, order.pk)
        )

        # 3. Set item as ended
        order = commands.create_order(client, item)
        response = self.client.post(
            '/bills/%s/orders/%s/' % (bill.pk, order.pk),
            dict(action='ended', item=order.item.pk, cancel=1)
        )
        self.assertRedirects(
            response, '/bills/%s/orders/%s/' % (bill.pk, order.pk)
        )
        order = Order.objects.get(pk=order.pk)
        self.assertEqual(order.status, Order.CANCELED)
        self.assertTrue(order.item.ended)
