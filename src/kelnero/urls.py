from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse_lazy

from django.contrib import admin
admin.autodiscover()


def _url(name, pat):
    return url(r'^%s$' % pat, name, name=name)


def client(name, pat):
    return url(r'^client/(?P<client_key>[a-z0-9]+)/%s$' % pat, name, name=name)


urlpatterns = patterns('kelnero.views',
    # Anonymous user urls.
    _url('home',             r''),
    _url('new_client',       r'bill/(?P<bill_code>\d{6})/new-client/'),

    # Client urls.
    client('items',          r'items/'),
    client('items',          r'items/(?P<parent>\d+)/'),
    client('client_orders',  r'orders/'),
    client('add_friend',     r'friends/add/'),
    client('friends',        r'friends/'),
    client('client_details', r'details/'),

    # Waiter urls.
    _url('waiter',           r'waiter/'),
    _url('bill_orders',      r'bills/(?P<bill_id>\d+)/orders/'),
    _url('order_details',    r'bills/(?P<bill_id>\d+)/orders/(?P<order_id>\d+)/'),

    _url('bills',            r'bills/'),
    _url('bill',             r'bills/(?P<bill_id>\d+)/'),
    _url('client_bill',      r'bills/(?P<bill_id>\d+)/clients/(?P<client_id>\d+)/'),

    url(r'^bills-paid/$', 'bills', {'show_paid': True}, name='paid_bills'),
    url(r'^bills-paid/(?P<bill_id>\d+)/$', 'bill', {'show_paid': True}, name='paid_bill'),
    url(r'^bills-paid/(?P<bill_id>\d+)/clients/(?P<client_id>\d+)/$', 'client_bill',
        {'show_paid': True}, name='paid_client_bill'),

    url(r'^admin/menu/', include('kelnero.manager.menu.urls')),
    url(r'^admin/codes/', include('kelnero.manager.codes.urls')),
    #url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns('',
    url(r'^logout/$', 'django.contrib.auth.views.logout', dict(
        next_page=reverse_lazy('waiter'),
    ), name='logout'),
)
