# coding: utf-8
from __future__ import unicode_literals

from django.utils.translation import ugettext

from crispy_forms.layout import HTML
from crispy_forms.utils import flatatt
from crispy_forms.bootstrap import StrictButton


class Submit(StrictButton):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('type', 'submit')
        kwargs.setdefault('css_class', 'btn-primary')
        super(Submit, self).__init__(*args, **kwargs)


class Cancel(HTML):
    def __init__(self, link):
        attrs = {
            'href': link,
            'class': 'btn btn-default',
        }
        super(Cancel, self).__init__('<a {attrs}>{label}</a>'.format(
            label=ugettext('Grįžti'),
            attrs=flatatt(attrs),
        ))
