import random
import string


def gen_code(size=6, alphabet=string.digits):
    return ''.join([random.choice(alphabet) for i in range(size)])
