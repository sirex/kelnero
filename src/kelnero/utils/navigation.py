import collections


NavItem = collections.namedtuple('NavItem',
    'key label link children right active'
)


def item(key, label='', link='', children=[], right=False, active=False):
    return NavItem(key, label, link, children, right, active)


def select_active(active, items):
    if active is None:
        return items

    updated_items = []
    active = active if isinstance(active, list) else [active]
    for item in items:
        if item.key in active:
            item = item._replace(active=True)
        updated_items.append(item)

    return updated_items
