import urllib


def referer(request):
    if 'ref' in request.GET:
        ref = request.GET['ref']
    else:
        ref = request.path
    return 'ref=%s' % urllib.quote(ref)
