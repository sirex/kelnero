from django.conf.urls import patterns
from django.conf.urls import url


def url_(name, pat):
    return url(r'^%s$' % pat, name, name=name)


def patterns_(views_module, *urls):
    url_prefix = views_module.replace('.', '_')
    return patterns('kelnero.%s.views' % views_module, *(
        url(r'^%s$' % pattern, name, name='%s_%s' % (url_prefix, name))
        for name, pattern in urls
    ))
