# coding: utf-8

from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.db.models import Sum
from django.contrib.auth.views import login

from .forms import EnterBillForm
from .forms import ExistingBillForm
from .forms import NewClientForm
from .forms import NewFriendForm
from .forms import LoginForm
from .models import Client
from .models import Bill
from .models import Item
from .models import Order
from . import navigation as nav
from . import commands
from . import queries


def home(request):
    if request.method == 'POST':
        action = request.POST.get('action')
        if action == 'new_client':
            new_client_form = EnterBillForm(request.POST)
            if new_client_form.is_valid():
                bill = Bill.objects.get(
                    code=new_client_form.cleaned_data['code']
                )
                return redirect('new_client', bill.code)
        else:
            new_client_form = EnterBillForm()

        if action == 'old_client':
            print('ok')
            old_client_form = ExistingBillForm(request.POST)
            if old_client_form.is_valid():
                client = (
                    Client.objects.get(
                        bill__code=old_client_form.cleaned_data['bill_code'],
                        code=old_client_form.cleaned_data['client_code'],
                    )
                )
                return redirect('client_orders', client.key)
        else:
            old_client_form = ExistingBillForm()

    else:
        new_client_form = EnterBillForm()
        old_client_form = ExistingBillForm()

    return render(request, 'home.html', dict(
        new_client_form=new_client_form,
        old_client_form=old_client_form,
    ))


def new_client(request, bill_code):
    bill = get_object_or_404(Bill, code=bill_code)
    if request.method == 'POST':
        form = NewClientForm(bill, request.POST)
        if form.is_valid():
            client = commands.create_client(bill, form.cleaned_data['name'])
            return redirect('client_orders', client.key)
    else:
        form = NewClientForm(bill)
    return render(request, 'new_client.html', dict(
        form=form,
    ))


def items(request, client_key, parent=None):
    client = get_object_or_404(Client, key=client_key)
    if not client.bill.active:
        return render(request, 'bill_closed.html')

    if parent is not None:
        parent = get_object_or_404(Item, pk=parent)

    if request.method == 'POST':
        item = get_object_or_404(Item, pk=request.POST.get('item'))
        commands.create_order(client, item)
        return redirect('client_orders', client.key)

    return render(request, 'items.html', dict(
        navbar=nav.client(client),
        client=client,
        parent=parent,
        items=Item.objects.filter(parent=parent, ended=False).order_by('price')
    ))


def client_orders(request, client_key):
    client = get_object_or_404(Client, key=client_key)
    if not client.bill.active:
        return render(request, 'bill_closed.html')

    if request.method == 'POST':
        action = request.POST.get('action')
        if action == 'confirm':
            commands.confirm_orders(client)
        elif 'delete' in request.POST:
            order = get_object_or_404(
                Order, client=client, status=Order.CREATED,
                pk=request.POST.get('delete', 0)
            )
            commands.delete_order(order)
        return redirect('client_orders', client.key)

    orders = Order.objects.filter(client=client)

    return render(request, 'client_orders.html', dict(
        navbar=nav.client(client, 'orders'),
        client=client,
        Order=Order,
        orders=orders,
        unaproved_orders=(
            Order.objects.filter(client=client, status=Order.CREATED).
            exists()
        ),
        total=(
            Order.objects.filter(client=client, status=Order.DELIVERED).
            aggregate(total=Sum('item__price')).get('total', 0) or 0
        ),
    ))


def add_friend(request, client_key):
    client = get_object_or_404(Client, key=client_key)
    if not client.bill.active:
        return render(request, 'bill_closed.html')

    if request.method == 'POST':
        form = NewFriendForm(client.bill, request.POST)
        if form.is_valid():
            commands.add_friend(client, form.cleaned_data['name'])
            return redirect('friends', client.key)
    else:
        form = NewFriendForm(client.bill)
    return render(request, 'new_client.html', dict(
        form=form,
    ))

    return render(request, '')


def friends(request, client_key):
    client = get_object_or_404(Client, key=client_key)
    if not client.bill.active:
        return render(request, 'bill_closed.html')

    return render(request, 'friends.html', dict(
        client=client,
        navbar=nav.client(client, 'friends'),
        friends=queries.query_all_friends(client),
    ))


def client_details(request, client_key):
    client = get_object_or_404(Client, key=client_key)
    if not client.bill.active:
        return render(request, 'bill_closed.html')

    return render(request, 'client_details.html', dict(
        client=client,
        navbar=nav.client(client, 'client'),
    ))


@login_required
def bills(request, show_paid=False):
    if show_paid:
        qry = dict(client__order__status=Order.PAID)
    else:
        qry = dict(active=True, client__order__status=Order.DELIVERED)

    return render(request, 'waiter/bills.html', dict(
        navbar=nav.waiter('bills'),
        show_paid=show_paid,
        bills=(
            Bill.objects.
            values('pk', 'code', 'table__name').
            filter(**qry).
            annotate(total=Sum('client__order__item__price'))
        ),
    ))


@login_required
def bill_orders(request, bill_id):
    bill = get_object_or_404(Bill, pk=bill_id)
    if request.method == 'POST':
        order = get_object_or_404(Order, pk=request.POST.get('order'))
        if order.status == Order.CONFIRMED:
            commands.update_order(order, status=Order.ACCEPTED)
        elif order.status == Order.ACCEPTED:
            commands.update_order(order, status=Order.DELIVERED)
        return redirect('bill_orders', bill.pk)

    return render(request, 'waiter/bill_orders.html')


@login_required
def bill(request, bill_id, show_paid=False):
    bill = get_object_or_404(Bill, pk=bill_id)

    if show_paid:
        status = Order.PAID
        redirect_url_name = 'paid_bill'
    else:
        status = Order.DELIVERED
        redirect_url_name = 'bill'

    if request.method == 'POST':
        action = request.POST.get('action')
        if action == 'paid':
            commands.bill_payment(bill)
            return redirect(redirect_url_name, bill.pk)
        elif action == 'open':
            commands.open_bill(bill)
            return redirect(redirect_url_name, bill.pk)
        elif action == 'close':
            commands.close_bill(bill)
            return redirect(redirect_url_name, bill.pk)

    return render(request, 'waiter/bill.html', dict(
        navbar=nav.waiter(),
        bill=bill,
        show_paid=show_paid,
        clients=(
            Client.objects.
            values('pk', 'code', 'name').
            filter(bill=bill, order__status=status).
            annotate(total=Sum('order__item__price'))
        ),
        total=(
            Client.objects.
            filter(bill=bill, order__status=status).
            aggregate(total=Sum('order__item__price')).get('total', 0) or 0
        ),
    ))


@login_required
def client_bill(request, bill_id, client_id, show_paid=False):
    bill = get_object_or_404(Bill, pk=bill_id)
    client = get_object_or_404(Client, bill=bill, pk=client_id)

    if show_paid:
        status = Order.PAID
        redirect_url_name = 'paid_client_bill'
    else:
        status = Order.DELIVERED
        redirect_url_name = 'client_bill'

    if request.method == 'POST':
        action = request.POST.get('action')
        if action == 'paid':
            commands.client_payment(client)
            return redirect(redirect_url_name, bill.pk, client.pk)

    return render(request, 'waiter/client_bill.html', dict(
        navbar=nav.waiter(),
        bill=bill,
        client=client,
        Order=Order,
        orders=(
            Order.objects.
            filter(
                client=client,
                status__gt=Order.CREATED
            )
        ),
        total=(
            Order.objects.
            filter(client=client, status=status).
            aggregate(total=Sum('item__price')).get('total', 0) or 0
        ),
        show_paid=show_paid,
    ))


def waiter(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            order = get_object_or_404(Order, pk=request.POST.get('order'))
            if order.status == Order.CONFIRMED:
                commands.update_order(order, status=Order.ACCEPTED)
            elif order.status == Order.ACCEPTED:
                commands.update_order(order, status=Order.DELIVERED)
            return redirect('waiter')

        return render(request, 'waiter/home.html', dict(
            navbar=nav.waiter('orders'),
            Order=Order,
            orders=(
                Order.objects.
                filter(status__gt=Order.CREATED, status__lt=Order.DELIVERED).
                order_by('-created')
            )
        ))
    else:
        return login(
            request, 'waiter/login.html', authentication_form=LoginForm
        )

@login_required
def order_details(request, bill_id, order_id):
    bill = get_object_or_404(Bill, pk=bill_id)
    order = get_object_or_404(Order, client__bill=bill, pk=order_id)

    if request.method == 'POST':
        action = request.POST.get('action')
        if action == 'cancel':
            commands.cancel_order(order, request.POST.get('reason', ''))
            return redirect('order_details', bill.pk, order.pk)
        elif action == 'ended':
            item = get_object_or_404(Item, pk=request.POST.get('item', 0))
            cancel_orders = request.POST.get('cancel', False)
            commands.set_items_ended(item, cancel_orders=cancel_orders)
            return redirect('order_details', bill.pk, order.pk)
        elif 'order' in request.POST:
            order = get_object_or_404(Order, pk=request.POST.get('order'))
            if order.status == Order.CONFIRMED:
                commands.update_order(order, status=Order.ACCEPTED)
            elif order.status == Order.ACCEPTED:
                commands.update_order(order, status=Order.DELIVERED)
            return redirect('order_details', bill.pk, order.pk)

    return render(request, 'waiter/order_details.html', dict(
        navbar=nav.waiter(),
        Order=Order,
        bill=bill,
        order=order,
    ))
